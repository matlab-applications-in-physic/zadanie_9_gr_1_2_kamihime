%{

Kamila Pajak
Engineering physics

Big data handling

%}


function [] = peakAnalysis(fetch_value)			% matlab function declaration

path = [‘Co60’, fetch_value]; 					% no absolute paths, although working on the third file
analyzed_file = fopen(path, 'r'); 			% open the file

fraction = 16777216; 				% can’t open the whole file  - 8 gb equals 8589934592 bytes divided into 512 parts
data = zeros(1, 255); 				% matlab equvalent for well-known python numpy.zeros


for i = 1:512 						% thanks to Agnieszka and Asia for idea for this loop
    values = fread(analyzed_file, fraction,’uint8');	 	% ‘uint8’ equals 1 byte
    for k = 1 : fraction;
        fetch_value(data(k)) = data(values(k)) + 1; 		% append to matrix
    end;
     
end;

P_1 = plot(data);						 % plotting the values 
xlabel(‘Values’);
ylabel(‘Occurence’);
title('Histogram');
values_1 = [fetch_value, ‘Histogram.pdf'];
saveas(Plot, values_1);


noiseLevel = NoiseLevel(data); 		% calling the function searching for noise
fprintf('Noise level: %d \n', noiseLevel);

