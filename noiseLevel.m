function [noise] = noiseLevel(data)					% function declaration

	[~, y_coordinate] = find(data == max(data));			% search along the maximum values
	noise = y_coordinate;
end
